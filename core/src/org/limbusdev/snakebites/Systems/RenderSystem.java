package org.limbusdev.snakebites.Systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import org.limbusdev.snakebites.Model.BlockComponent;
import org.limbusdev.snakebites.Model.Direction;
import org.limbusdev.snakebites.Model.FoodComponent;
import org.limbusdev.snakebites.Model.GameState;
import org.limbusdev.snakebites.Model.GameStateObserver;
import org.limbusdev.snakebites.Model.IntVector2;
import org.limbusdev.snakebites.Model.PathComponent;
import org.limbusdev.snakebites.Model.PositionComponent;
import org.limbusdev.snakebites.Model.SnakeEntity;
import org.limbusdev.snakebites.Model.ThemePreset;
import org.limbusdev.snakebites.THEME;
import org.limbusdev.snakebites.util.GlobalSettings;
import org.limbusdev.snakebites.util.MediaManager;

/**
 * Created by georg on 12.05.16.
 */
public class RenderSystem extends EntitySystem implements EntityListener, GameStateObserver{
    private ImmutableArray<Entity> entities;
    private ImmutableArray<Entity> food;
    private ImmutableArray<Entity> blocks;

    private TextureRegion apple,wall;
    private Array<TextureRegion> snakeHead;
    private Array<TextureRegion> snakeNeck;
    private Array<TextureRegion> snakeRound;
    private Array<TextureRegion> snakeTail;
    private TextureRegion bg;
    private Stage stage;
    private Skin skin;
    private Label labelScore, labelPause;
    private MediaManager media;
    private int offX, offY, resTile;
    private SnakeEntity snake;
    private EntityComponentSystem ECS;

    public RenderSystem(MediaManager media, ThemePreset theme, EntityComponentSystem ECS) {

        this.ECS = ECS;
        ECS.gameState.addObserver(this);
        FitViewport fit = new FitViewport(GlobalSettings.SCREEN_WIDTH, GlobalSettings.SCREEN_HEIGHT);
        this.stage = new Stage(fit);
        this.skin = media.skin;

        this.offX = 8;
        this.offY = 578;
        this.resTile = 32;

        this.media = media;
        TextureAtlas atlas;

        switch(theme.theme) {
            case TOON:
                atlas = media.getTextureAtlas(media.snakeToon);
                break;
            case PIXELS:
                atlas = media.getTextureAtlas(media.snakePixels);
                break;
            default:
                atlas = media.getTextureAtlas(media.snakeRetro);
                break;
        }
        apple = atlas.findRegion("apple");
        wall = atlas.findRegion("wall");
        snakeHead = new Array<TextureRegion>();
        snakeNeck = new Array<TextureRegion>();
        snakeRound = new Array<TextureRegion>();
        snakeTail = new Array<TextureRegion>();
        snakeHead.add(atlas.findRegion("head_n"));
        snakeHead.add(atlas.findRegion("head_e"));
        snakeHead.add(atlas.findRegion("head_s"));
        snakeHead.add(atlas.findRegion("head_w"));
        snakeNeck.add(atlas.findRegion("neck_v"));
        snakeNeck.add(atlas.findRegion("neck_h"));
        snakeRound.add(atlas.findRegion("round", 1));
        snakeRound.add(atlas.findRegion("round", 2));
        snakeRound.add(atlas.findRegion("round", 3));
        snakeRound.add(atlas.findRegion("round", 4));
        snakeTail.add(atlas.findRegion("tail_n"));
        snakeTail.add(atlas.findRegion("tail_e"));
        snakeTail.add(atlas.findRegion("tail_s"));
        snakeTail.add(atlas.findRegion("tail_w"));
        bg = (atlas.findRegion("bg"));

        labelScore = new Label("",skin);
        Label.LabelStyle ls = new Label.LabelStyle();
        ls.font = media.skin.getFont(theme.screenFontColor);
        labelScore.setStyle(ls);
        labelScore.setWidth(256);labelScore.setHeight(96);labelScore.setPosition(600, 1180);
        this.stage.addActor(labelScore);

        labelPause = new Label("",skin);
        labelPause.setStyle(ls);
        labelPause.setWidth(256);labelPause.setHeight(96);
        labelPause.setPosition(GlobalSettings.SCREEN_WIDTH/2, GlobalSettings.SCREEN_HEIGHT*3/4, Align.center);
        this.stage.addActor(labelPause);
    }

    public void render(Batch batch, ShapeRenderer shp) {

        batch.begin();
        batch.draw(bg, 40, 610);
        batch.end();

        renderToon(batch);

        this.stage.draw();
    }

    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(PathComponent.class).get());
        food     = engine.getEntitiesFor(Family.all(FoodComponent.class).get());
        snake    = (SnakeEntity) entities.get(0);
        blocks   = engine.getEntitiesFor(Family.all(BlockComponent.class).get());
    }

    public void update(float deltaTime) {
        for (Entity entity : entities) {
            // TODO
        }
        stage.act(deltaTime);
    }

    private void renderToon(Batch batch) {

        if(ECS.gameState.paused) return;

        batch.begin();
        for(Entity f : food) {
            PositionComponent pos = f.getComponent(PositionComponent.class);
            batch.draw(apple, pos.x*resTile+offX, pos.y*resTile+offY);
        }
        for(Entity block : blocks) {
            BlockComponent pc = block.getComponent(BlockComponent.class);
            batch.draw(wall, pc.pos.x*resTile+offX, pc.pos.y*resTile+offY);
        }


        for (Entity entity : entities) {
            PathComponent pc = entity.getComponent(PathComponent.class);
            IntVector2 lastVec = pc.points.getFirst();
            for(IntVector2 v : pc.points) {

                TextureRegion tex = apple; // place holder
                if(v.equals(pc.points.getLast())) {
                    // HEAD
                    IntVector2 neck = pc.points.get(pc.points.size()-2);
                    if(neck.x != v.x) {
                        // Horizontal
                        if(neck.x > v.x) {
                            if(Math.abs(neck.x-v.x) < 2) {
                                // Left
                                tex = snakeHead.get(3);
                            } else {
                                // Right
                                tex = snakeHead.get(1);
                            }
                        }
                        if(neck.x < v.x) {
                            if(Math.abs(neck.x-v.x) < 2) {
                                // Right
                                tex = snakeHead.get(1);
                            } else {
                                // Left
                                tex = snakeHead.get(3);
                            }
                        }
                    } else {
                        // Vertical
                        if(neck.y > v.y) {
                            if(Math.abs(neck.y-v.y) < 2) {
                                // Down
                                tex = snakeHead.get(2);
                            } else {
                                // Up
                                tex = snakeHead.get(0);
                            }
                        }
                        if(neck.y < v.y) {
                            if(Math.abs(neck.y-v.y) < 2) {
                                // Up
                                tex = snakeHead.get(0);
                            } else {
                                // Down
                                tex = snakeHead.get(2);
                            }
                        }
                    }
                } else {
                    // Not Head
                    if(v.equals(pc.points.getFirst())) {
                        // Tail
                        IntVector2 neck = pc.points.get(1);
                        if(neck.x != v.x) {
                            // Horizontal
                            if(neck.x > v.x) {
                                if(Math.abs(neck.x-v.x) < 2) {
                                    // Left
                                    tex = snakeTail.get(3);
                                } else {
                                    // Right
                                    tex = snakeTail.get(1);
                                }
                            } else {
                                if(Math.abs(neck.x-v.x) < 2) {
                                    // Right
                                    tex = snakeTail.get(1);
                                } else {
                                    // Left
                                    tex = snakeTail.get(3);
                                }
                            }
                        } else {
                            // Vertical
                            if(neck.y > v.y) {
                                if(Math.abs(neck.y-v.y) < 2) {
                                    // Down
                                    tex = snakeTail.get(2);
                                } else {
                                    // Up
                                    tex = snakeTail.get(0);
                                }
                            } else {
                                if(Math.abs(neck.y-v.y) < 2) {
                                    // Up
                                    tex = snakeTail.get(0);
                                } else {
                                    // Down
                                    tex = snakeTail.get(2);
                                }
                            }
                        }
                    } else {
                        // Not Head Nor Tail - Body
                        IntVector2 nextVec = pc.points.get(pc.points.indexOf(v)+1);
                        if(!(lastVec.x != nextVec.x &&
                             lastVec.y != nextVec.y)) {
                            // Body Straight
                            if(v.x != lastVec.x) {
                                // Horizontal
                                tex = snakeNeck.get(1);
                            } else {
                                // Vertical
                                tex = snakeNeck.get(0);
                            }
                        } else {
                            // ............................................................... ROUND
                            Direction bodyDir = Direction.NE;
                            // Body Round
                            if(nextVec.x > lastVec.x) {
                                if(Math.abs(nextVec.x-lastVec.x)<2) {
                                    // Right
                                    if(nextVec.y > lastVec.y) {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Right > Up
                                            bodyDir = Direction.NE;
                                        } else {
                                            // Right > Down
                                            bodyDir = Direction.SE;
                                        }
                                    } else {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Right > Down
                                            bodyDir = Direction.SE;
                                        } else {
                                            // Right > Up
                                            bodyDir = Direction.NE;
                                        }
                                    }
                                } else {
                                    // Left
                                    if(nextVec.y > lastVec.y) {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Left > Up
                                            bodyDir = Direction.NW;
                                        } else {
                                            // Left > Down
                                            bodyDir = Direction.SW;
                                        }
                                    } else {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Left > Down
                                            bodyDir = Direction.SW;
                                        } else {
                                            // Left > Up
                                            bodyDir = Direction.NW;
                                        }
                                    }
                                }
                            } else {
                                if(Math.abs(nextVec.x-lastVec.x)<2) {
                                    // Left
                                    if(nextVec.y > lastVec.y) {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Left > Up
                                            bodyDir = Direction.NW;
                                        } else {
                                            // Left > Down
                                            bodyDir = Direction.SW;
                                        }
                                    } else {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Left > Down
                                            bodyDir = Direction.SW;
                                        } else {
                                            // Left > Up
                                            bodyDir = Direction.NW;
                                        }
                                    }
                                } else {
                                    // Right
                                    if(nextVec.y > lastVec.y) {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Right > Up
                                            bodyDir = Direction.NE;
                                        } else {
                                            // Right > Down
                                            bodyDir = Direction.SE;
                                        }
                                    } else {
                                        if(Math.abs(nextVec.y-lastVec.y)<2) {
                                            // Right > Down
                                            bodyDir = Direction.SE;
                                        } else {
                                            // Right > Up
                                            bodyDir = Direction.NE;
                                        }
                                    }
                                }
                            }
                            // ............................................................... ROUND
                            switch(bodyDir) {
                                case NE:
                                    if(v.x == lastVec.x) {
                                        tex = snakeRound.get(1);
                                    } else {
                                        tex = snakeRound.get(3);
                                    }break;
                                case NW:
                                    if(v.y == lastVec.y) {
                                        tex = snakeRound.get(0);
                                    } else {
                                        tex = snakeRound.get(2);
                                    }break;
                                case SE:
                                    if(v.x == lastVec.x) {
                                        tex = snakeRound.get(0);
                                    } else {
                                        tex = snakeRound.get(2);
                                    }break;
                                default:
                                    if(v.x == lastVec.x) {
                                        tex = snakeRound.get(3);
                                    } else {
                                        tex = snakeRound.get(1);
                                    }break;
                            }
                        }
                    }
                }
                batch.draw(tex, v.x*resTile+offX, v.y*resTile+offY);
                lastVec = v;
            }
        }
        batch.end();
    }

    @Override
    public void entityAdded(Entity entity) {
        this.addedToEngine(getEngine());
    }

    @Override
    public void entityRemoved(Entity entity) {
        this.addedToEngine(getEngine());
    }

    public void resize(int width, int height) {
        stage.getViewport().update(width,height);
    }

    @Override
    public void getNotified(GameState gameState) {
        this.labelScore.setText(Integer.toString(gameState.score));
        if(gameState.paused || gameState.gameOver || gameState.levelUp) {
            // PAUSE
            if(gameState.paused && !gameState.gameOver)this.labelPause.setText("PAUSE");

            // GAME OVER
            if(gameState.gameOver) this.labelPause.setText("GAME OVER");

            // LEVEL UP
            if(gameState.levelUp) {
                if(gameState.level==GlobalSettings.MAX_LEVEL) {
                    this.labelPause.setText("CONGRATULATIONS!\n\nYou cleared all levels!");
                } else {
                    if(gameState.level%10==0) {
                        this.labelPause.setText("AWESOME!\n\nLevel " + gameState.level + " clear!");
                    } else {
                        this.labelPause.setText("COMPLETE\n\nGo to level " + gameState.level + "!");
                    }

                }

            }

            labelPause.setEllipsis(true);
            labelPause.setAlignment(Align.center, Align.center);
            labelPause.setPosition(
                    GlobalSettings.SCREEN_WIDTH/2-labelPause.getGlyphLayout().width/2,
                    GlobalSettings.SCREEN_HEIGHT*3/4, Align.center);
        }
        else this.labelPause.setText("");
    }
}
