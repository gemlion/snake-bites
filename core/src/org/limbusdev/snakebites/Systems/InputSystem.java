package org.limbusdev.snakebites.Systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import org.limbusdev.snakebites.Model.Direction;
import org.limbusdev.snakebites.Model.GameMode;
import org.limbusdev.snakebites.Model.GameState;
import org.limbusdev.snakebites.Model.GameStateObserver;
import org.limbusdev.snakebites.Model.InputComponent;
import org.limbusdev.snakebites.Model.IntVector2;
import org.limbusdev.snakebites.Model.PathComponent;
import org.limbusdev.snakebites.Model.ThemePreset;
import org.limbusdev.snakebites.Screens.InGameScreen;
import org.limbusdev.snakebites.Screens.MainMenuScreen;
import org.limbusdev.snakebites.SnakeBites;
import org.limbusdev.snakebites.util.GlobalSettings;

import java.util.LinkedList;

/**
 * Created by georg on 12.05.16.
 */
public class InputSystem extends EntitySystem implements InputProcessor, GameStateObserver {

    private Rectangle touchPadArea;
    private Viewport viewport;
    private Vector2 touchPos;
    private ImmutableArray<Entity> entities;
    private Array<TextureRegion> controlImgs;
    private TextureRegion currentControlImg, screenGlass;
    private Stage stage;
    private Skin skin;
    private final SnakeBites game;
    private PathComponent snakePath;
    private InputComponent snakeInput;
    private EntityComponentSystem ECS;
    private Button buttonHome, buttonPause;
    private TextButton buttonNext, buttonQuit;
    private ThemePreset theme;


    public InputSystem(final Viewport viewport, final SnakeBites game, ThemePreset theme, final EntityComponentSystem ECS) {

        this.theme = theme;
        this.ECS = ECS;
        this.ECS.gameState.addObserver(this);
        this.game=game;

        TextureAtlas atlas = game.media.getTextureAtlas(theme.spriteSheetFile);
        this.controlImgs = new Array<TextureRegion>();
        this.controlImgs.add(atlas.findRegion("control_idle"));
        this.controlImgs.add(atlas.findRegion("control_up"));
        this.controlImgs.add(atlas.findRegion("control_right"));
        this.controlImgs.add(atlas.findRegion("control_down"));
        this.controlImgs.add(atlas.findRegion("control_left"));
        this.controlImgs.add(atlas.findRegion("screen"));
        this.screenGlass = controlImgs.get(5);

        // Scene2D
        FitViewport fit = new FitViewport(GlobalSettings.SCREEN_WIDTH, GlobalSettings.SCREEN_HEIGHT);
        this.stage = new Stage(fit);
        this.skin = game.media.skin;
        this.touchPadArea = new Rectangle(160,60,400,400);


        buttonHome = new Button(skin);
        Button.ButtonStyle buttonBackStyle = new Button.ButtonStyle();
        TextureRegionDrawable buttonHomeImgUp = new TextureRegionDrawable(atlas.findRegion("buttonHome_up"));
        TextureRegionDrawable buttonHomeImgDown = new TextureRegionDrawable(atlas.findRegion("buttonHome_down"));
        buttonBackStyle.up   = buttonHomeImgUp;
        buttonBackStyle.down = buttonHomeImgDown;
        buttonHome.setStyle(buttonBackStyle);
        buttonHome.setPosition(10,GlobalSettings.SCREEN_HEIGHT-20
                -screenGlass.getRegionHeight()-10-buttonHomeImgUp.getRegion().getRegionHeight());
        buttonHome.setWidth(buttonHomeImgUp.getRegion().getRegionWidth());
        buttonHome.setHeight(buttonHomeImgUp.getRegion().getRegionHeight());
        buttonHome.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MainMenuScreen(game, ECS.gameState.mode));
            }
        });
        this.stage.addActor(buttonHome);

        buttonPause = new Button(skin);
        Button.ButtonStyle buttonPauseStyle = new Button.ButtonStyle();
        buttonPauseStyle.up   = new TextureRegionDrawable(atlas.findRegion("buttonPause_up"));
        buttonPauseStyle.down = new TextureRegionDrawable(atlas.findRegion("buttonPause_down"));
        buttonPause.setStyle(buttonPauseStyle);
        buttonPause.setPosition(GlobalSettings.SCREEN_WIDTH-106,GlobalSettings.SCREEN_HEIGHT-20
                -screenGlass.getRegionHeight()-10-buttonHomeImgUp.getRegion().getRegionHeight());
        buttonPause.setWidth(buttonHomeImgUp.getRegion().getRegionWidth());
        buttonPause.setHeight(buttonHomeImgUp.getRegion().getRegionHeight());
        buttonPause.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(!ECS.gameState.paused) ECS.gameState.setPaused(true);
                else ECS.gameState.setPaused(false);
            }
        });
        this.stage.addActor(buttonPause);

        buttonNext = new TextButton("Next", skin);
        TextButton.TextButtonStyle buttonNextStyle = new TextButton.TextButtonStyle();
        buttonNextStyle.up   = new TextureRegionDrawable(atlas.findRegion("buttonLong_up"));
        buttonNextStyle.down = new TextureRegionDrawable(atlas.findRegion("buttonLong_down"));
        buttonNextStyle.font = skin.getFont(theme.buttonFontColor);
        buttonNext.setStyle(buttonNextStyle);
        buttonNext.setPosition(GlobalSettings.SCREEN_WIDTH/2-128,256);
        buttonNext.setWidth(atlas.findRegion("buttonLong_up").getRegionWidth());
        buttonNext.setHeight(atlas.findRegion("buttonLong_up").getRegionHeight());

        buttonQuit = new TextButton("Quit", skin);
        buttonQuit.setStyle(buttonNextStyle);
        buttonQuit.setPosition(GlobalSettings.SCREEN_WIDTH/2-128,128);
        buttonQuit.setWidth(atlas.findRegion("buttonLong_up").getRegionWidth());
        buttonQuit.setHeight(atlas.findRegion("buttonLong_up").getRegionHeight());

        this.viewport = viewport;
        this.touchPos = new Vector2(0,0);

        this.setInputProcessor();
        this.currentControlImg = controlImgs.first();
    }

    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(PathComponent.class, InputComponent.class).get());
        this.snakePath = entities.get(0).getComponent(PathComponent.class);
        this.snakeInput = entities.get(0).getComponent(InputComponent.class);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(ECS.gameState.paused || ECS.gameState.levelUp || ECS.gameState.gameOver) {
            //System.out.println("Steering not available.");
            return false;
        }

        this.touchPos.x = screenX;
        this.touchPos.y = screenY;
        this.touchPos = viewport.unproject(touchPos);

        if(!touchPadArea.contains(touchPos)) return false;

        float diffX, diffY;
        diffX = GlobalSettings.SCREEN_WIDTH/2  - touchPos.x;
        diffY = touchPadArea.y + touchPadArea.height/2 - touchPos.y;

        //System.out.println(touchPos);
        boolean moveLegal = false;
        if (Math.abs(diffX) >= Math.abs(diffY)) {
            // Left or Right
            if (diffX >= 0) {
                // Left
                //System.out.println("Left");
                if (moveLegal = moveLegal(Direction.W, snakePath.points)) snakePath.dir = Direction.W;
                currentControlImg = controlImgs.get(4);
            } else {
                // Right
                //System.out.println("Right");
                if (moveLegal = moveLegal(Direction.E, snakePath.points)) snakePath.dir = Direction.E;
                currentControlImg = controlImgs.get(2);
            }
        } else {
            // Up or Down
            if (diffY >= 0) {
                // Down
                //System.out.println("Down");
                if (moveLegal = moveLegal(Direction.S, snakePath.points)) snakePath.dir = Direction.S;
                currentControlImg = controlImgs.get(3);
            } else {
                // Up
                //System.out.println("Up");
                if (moveLegal = moveLegal(Direction.N, snakePath.points)) snakePath.dir = Direction.N;
                currentControlImg = controlImgs.get(1);
            }
        }
        if(moveLegal) snakeInput.active =true;
        return true;
    }

    private boolean moveLegal(Direction dir, LinkedList<IntVector2> path) {
        boolean legal=true;

        Direction currentImpossibleDir = Direction.N;

        if(path.getLast().x != path.get(path.size()-2).x) {
            // Hor
            if(path.getLast().x < path.get(path.size()-2).x)
                currentImpossibleDir = Direction.E;
            else
                currentImpossibleDir = Direction.W;
        } else {
            // Ver
            if(path.getLast().y < path.get(path.size()-2).y)
                currentImpossibleDir = Direction.N;
            else
                currentImpossibleDir = Direction.S;
        }

        if(dir == currentImpossibleDir || dir == counterDirectionOf(currentImpossibleDir)) legal = false;

        return legal;
    }

    public Direction counterDirectionOf(Direction dir) {
        switch(dir) {
            case N: return Direction.S;
            case S: return Direction.N;
            case E: return Direction.W;
            default: return Direction.E;

        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(!touchPadArea.contains(touchPos)) return false;
        this.currentControlImg = this.controlImgs.get(0);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        this.touchDown(screenX, screenY, pointer, 0);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void render(Batch batch, ShapeRenderer shp) {
        batch.begin();
        batch.draw(this.screenGlass, 0, 1280-screenGlass.getRegionHeight()-18);
        if(!ECS.gameState.paused)batch.draw(this.currentControlImg, touchPadArea.x, touchPadArea.y);
        batch.end();


        shp.begin(ShapeRenderer.ShapeType.Line);
        shp.setColor(Color.GREEN);
        shp.end();

        stage.draw();
    }

    @Override
    public void update(float delta) {
        stage.act(delta);

    }

    public void resize(int width, int height) {
        stage.getViewport().update(width,height);
    }

    public void setInputProcessor() {
        InputMultiplexer im = new InputMultiplexer();
        im.addProcessor(stage);
        im.addProcessor(this);

        Gdx.input.setInputProcessor(im);
    }

    @Override
    public void getNotified(final GameState gameState) {

        if(gameState.gameOver || gameState.levelUp) {
            // Hide Pause Button
            buttonPause.setVisible(false);
        }

        if(gameState.gameOver && !(gameState.level==GlobalSettings.MAX_LEVEL)) {
            buttonHome.setVisible(false);

            stage.addActor(buttonQuit);
            if(gameState.mode == GameMode.LEVELS) {
                stage.addActor(buttonNext);
                buttonNext.addAction(Actions.sequence(Actions.visible(false), Actions.alpha(0)));
                buttonNext.act(1);
                buttonNext.addAction(Actions.sequence(Actions.delay(1.5f),Actions.visible(true), Actions.fadeIn(1)));
                buttonNext.setText("Again");
                buttonNext.addListener(
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                System.out.println("Again!");
                                GameState state = new GameState(gameState.mode);
                                game.setScreen(new InGameScreen(game, theme, state));
                                submitScore(gameState);
                                unlockAchievement(gameState);
                            }
                        }
                );
            }

            buttonQuit.addAction(Actions.sequence(Actions.visible(false), Actions.alpha(0)));
            buttonQuit.act(1);
            buttonQuit.addAction(Actions.sequence(Actions.delay(1.5f),Actions.visible(true), Actions.fadeIn(1)));
            buttonQuit.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        game.setScreen(new MainMenuScreen(game, gameState.mode));
                        submitScore(gameState);
                        unlockAchievement(gameState);
                    }
                }
            );
        }

        if(gameState.levelUp && !(gameState.level==GlobalSettings.MAX_LEVEL)) {
            System.out.println("Level Up!");
            stage.addActor(buttonNext);
            buttonNext.addAction(Actions.sequence(Actions.visible(false), Actions.alpha(0)));
            buttonNext.act(1);
            buttonNext.addAction(Actions.sequence(Actions.delay(1.5f),Actions.visible(true), Actions.fadeIn(1)));
            buttonNext.addListener(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            setUpNextLevel();
                        }
                    }
            );
        }

        if(gameState.level==GlobalSettings.MAX_LEVEL) {
            stage.addActor(buttonNext);
            buttonNext.setText("to Menu");
            buttonNext.addAction(Actions.sequence(Actions.visible(false), Actions.alpha(0)));
            buttonNext.act(1);
            buttonNext.addAction(Actions.sequence(Actions.delay(1.5f),Actions.visible(true), Actions.fadeIn(1)));
            buttonNext.addListener(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            game.setScreen(new MainMenuScreen(game, GameMode.LEVELS));
                            submitScore(gameState);
                            unlockAchievement(gameState);
                        }
                    }
            );
        }
    }

    private void setUpNextLevel() {
        GameState gs = ECS.gameState;
        gs.gameOver=false;
        gs.paused = false;
        gs.levelUp = false;
        game.setScreen(new InGameScreen(game, theme, gs));
    }

    private void submitScore(GameState gameState) {
        System.out.println(gameState.score);
    }

    private void unlockAchievement(final GameState gameState) {
        System.out.println("Level: " + gameState.level);
    }
}
