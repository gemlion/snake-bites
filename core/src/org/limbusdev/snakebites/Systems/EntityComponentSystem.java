package org.limbusdev.snakebites.Systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.Viewport;

import org.limbusdev.snakebites.Model.BlockComponent;
import org.limbusdev.snakebites.Model.Direction;
import org.limbusdev.snakebites.Model.FoodComponent;
import org.limbusdev.snakebites.Model.GameState;
import org.limbusdev.snakebites.Model.InputComponent;
import org.limbusdev.snakebites.Model.IntVector2;
import org.limbusdev.snakebites.Model.PathComponent;
import org.limbusdev.snakebites.Model.SnakeEntity;
import org.limbusdev.snakebites.Model.ThemePreset;
import org.limbusdev.snakebites.Screens.InGameScreen;
import org.limbusdev.snakebites.SnakeBites;

/**
 * Created by georg on 12.05.16.
 */
public class EntityComponentSystem {
    private Engine engine;
    public   SnakeBites game;
    private Entity snake;
    private InGameScreen igs;
    private Viewport viewport;
    public GameState gameState;

    public EntityComponentSystem(SnakeBites game, InGameScreen igs, Viewport viewport, ThemePreset theme, GameState gameState) {
        this.viewport = viewport;
        this.game = game;
        this.igs = igs;
        this.engine = new Engine();
        this.gameState = gameState;
        //this.gameState.level = 14;
        createSnake();
        setUpSystems(theme);
    }

    public void createSnake() {
        this.snake = new SnakeEntity();
        this.snake.add(new PathComponent(new IntVector2(5, 4), 5));
        this.snake.add(new InputComponent(Direction.E));
        engine.addEntity(snake);
    }

    public void update(float delta) {
        if(!gameState.paused && !gameState.gameOver) {
            this.engine.update(delta);
        } else {
            this.engine.getSystem(InputSystem.class).update(delta);
        }
    }

    public void render(Batch batch, ShapeRenderer shp) {
        engine.getSystem(InputSystem.class).render(batch, shp);
        engine.getSystem(RenderSystem.class).render(batch, shp);

    }

    public void setUpSystems(ThemePreset theme) {
        BlockSystem blockSystem = new BlockSystem(gameState.level, engine);
        blockSystem.addedToEngine(engine);
        engine.addSystem(blockSystem);

        MovementSystem movementSystem = new MovementSystem(this);
        movementSystem.addedToEngine(engine);
        engine.addSystem(movementSystem);

        InputSystem inputSystem = new InputSystem(viewport, game, theme, this);
        inputSystem.addedToEngine(engine);
        engine.addSystem(inputSystem);

        FoodSystem foodSystem = new FoodSystem(engine, this);
        foodSystem.addedToEngine(engine);
        engine.addSystem(foodSystem);

        RenderSystem renderSystem = new RenderSystem(game.media,theme, this);
        renderSystem.addedToEngine(engine);
        engine.addSystem(renderSystem);

        engine.addEntityListener(Family.all(FoodComponent.class, PathComponent.class).get(), foodSystem);
        engine.addEntityListener(Family.all(FoodComponent.class, PathComponent.class).get(), renderSystem);
        engine.addEntityListener(Family.all(FoodComponent.class).get(), movementSystem);
        engine.addEntityListener(Family.all(BlockComponent.class).get(), blockSystem);
    }

    public void setInputProcessor() {
        engine.getSystem(InputSystem.class).setInputProcessor();
    }

    public void resize(int width, int height) {
        engine.getSystem(InputSystem.class).resize(width, height);
        engine.getSystem(RenderSystem.class).resize(width,height);
    }
}
