package org.limbusdev.snakebites.Systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;

import org.limbusdev.snakebites.Model.BlockComponent;
import org.limbusdev.snakebites.Model.IntVector2;
import org.limbusdev.snakebites.util.LevelProducer;

/**
 * Created by georg on 16.05.16.
 */
public class BlockSystem extends EntitySystem implements EntityListener{

    private ImmutableArray<Entity> blocks;

    public BlockSystem(int level, Engine engine) {
        for(int i=1; i<=20; i++) {
            for(int j=1; j<=20; j++) {
                if(LevelProducer.getLevelBlocks(level)[i-1][j-1] == 1) {
                    Entity block = new Entity();
                    block.add(new BlockComponent(new IntVector2(i,j)));
                    engine.addEntity(block);
                }
            }
        }
    }

    public void addedToEngine(Engine engine) {
        blocks = engine.getEntitiesFor(Family.all(BlockComponent.class).get());
    }

    public void update(float deltaTime) {
        // TODO
    }


    @Override
    public void entityAdded(Entity entity) {
        this.addedToEngine(this.getEngine());
    }

    @Override
    public void entityRemoved(Entity entity) {
        this.addedToEngine(this.getEngine());
    }
}
