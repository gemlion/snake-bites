package org.limbusdev.snakebites.Systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.utils.TimeUtils;

import org.limbusdev.snakebites.Model.BlockComponent;
import org.limbusdev.snakebites.Model.InputComponent;
import org.limbusdev.snakebites.Model.IntVector2;
import org.limbusdev.snakebites.Model.PathComponent;
import org.limbusdev.snakebites.util.GlobalSettings;


/**
 * Created by georg on 12.05.16.
 */
public class MovementSystem extends EntitySystem implements EntityListener {

    private Entity snake;
    private ImmutableArray<Entity> entities;
    private ImmutableArray<Entity> blocks;
    private long lastStepTime;
    private long timeBetweenSteps=250;
    private IntVector2 newVertex;
    private EntityComponentSystem ecs;

    public MovementSystem(EntityComponentSystem ecs) {
        this.lastStepTime = TimeUtils.millis();
        newVertex = new IntVector2(0,0);
        this.ecs = ecs;
    }

    public void addedToEngine(Engine engine) {
        this.snake = engine.getEntities().first();
        entities   = engine.getEntitiesFor(Family.all(PathComponent.class, InputComponent.class).get());
        blocks     = engine.getEntitiesFor(Family.all(BlockComponent.class).get());
    }

    public void update(float deltaTime) {
        // check for game over
        PathComponent pc = snake.getComponent(PathComponent.class);
        InputComponent ic = snake.getComponent(InputComponent.class);
        for(IntVector2 i : pc.points) {
            if(i.x == pc.points.getLast().x && i.y == pc.points.getLast().y && !i.equals(pc.points.getLast())) {
                // Snake ate her tail
                ecs.gameState.setGameOver(true);
            }
            for(Entity block : blocks) {
                BlockComponent bc = block.getComponent(BlockComponent.class);
                if(bc.pos.x == pc.points.getLast().x && bc.pos.y == pc.points.getLast().y) {
                    ecs.gameState.setGameOver(true);
                }
            }
        }

        if((ic.active &&  !(TimeUtils.timeSinceMillis(lastStepTime) < timeBetweenSteps*.6))|| TimeUtils.timeSinceMillis(lastStepTime) > timeBetweenSteps) {
            ic.active = false;
            lastStepTime = TimeUtils.millis();
                newVertex.x = pc.points.getLast().x;
                newVertex.y = pc.points.getLast().y;

                switch (pc.dir) {
                    case N:
                        newVertex.y+=1;
                        break;
                    case E:
                        newVertex.x+=1;
                        break;
                    case S:
                        newVertex.y-=1;
                        break;
                    default:
                        newVertex.x-=1;
                        break;
                }
                if(newVertex.x>20)newVertex.x=1;
                if(newVertex.x<1)newVertex.x=20;
                if(newVertex.y>20)newVertex.y=1;
                if(newVertex.y<1)newVertex.y=20;

                pc.points.addLast(new IntVector2(newVertex.x, newVertex.y));
                if(!pc.points.isEmpty()) {
                    if(pc.inBelly<1) pc.points.removeFirst();
                    else pc.inBelly--;
                }
        }
    }

    @Override
    public void entityAdded(Entity entity) {
        // TODO
    }

    @Override
    public void entityRemoved(Entity entity) {
        // If snake eats an apple
        if(timeBetweenSteps > 50) this.timeBetweenSteps-= GlobalSettings.TIME_BETWEEN_STEPS_DECREASE;
        //System.out.println("New Snake Speed: " + timeBetweenSteps);
        ecs.gameState.raiseScore(5);
    }
}
