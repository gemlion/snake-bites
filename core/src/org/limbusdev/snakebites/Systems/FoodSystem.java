package org.limbusdev.snakebites.Systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;

import org.limbusdev.snakebites.Model.BlockComponent;
import org.limbusdev.snakebites.Model.FoodComponent;
import org.limbusdev.snakebites.Model.GameMode;
import org.limbusdev.snakebites.Model.InputComponent;
import org.limbusdev.snakebites.Model.IntVector2;
import org.limbusdev.snakebites.Model.PathComponent;
import org.limbusdev.snakebites.Model.PositionComponent;
import org.limbusdev.snakebites.util.GlobalSettings;

/**
 * Created by georg on 13.05.16.
 */
public class FoodSystem extends EntitySystem implements EntityListener{
    private ImmutableArray<Entity> food;
    private ImmutableArray<Entity> blocks;
    private Entity snake;
    private EntityComponentSystem ECS;
    private int foodCounter;
    private Engine engine;
    private Sound eatSound;

    public FoodSystem(Engine engine, EntityComponentSystem ecs) {
        this.foodCounter = 0;
        this.ECS = ecs;
        this.engine = engine;
        this.eatSound = this.ECS.game.media.assets.get(ecs.game.media.sfx.get(0), Sound.class);
    }

    public void addedToEngine(Engine engine) {
        food = engine.getEntitiesFor(Family.all(PositionComponent.class, FoodComponent.class).get());
        blocks = engine.getEntitiesFor(Family.all(BlockComponent.class).get());
        snake = engine.getEntitiesFor(Family.all(PathComponent.class, InputComponent.class).get()).first();
    }

    public void update(float deltaTime) {
        if(foodCounter == 0) spawnFood();

        PathComponent pc = snake.getComponent(PathComponent.class);
        for(Entity e : food) {
            PositionComponent pos = e.getComponent(PositionComponent.class);
            FoodComponent food = e.getComponent(FoodComponent.class);
            if(pc.points.getLast().x == pos.x && pc.points.getLast().y == pos.y) {
                //eatSound.play();
                pc.eat(food.value);
                this.getEngine().removeEntity(e);
                spawnFood();
            }
        }
    }

    public void spawnFood() {
        boolean spawnedFood = false;
        IntVector2 pos = new IntVector2(0,0);
        while(!spawnedFood) {
            pos = new IntVector2(MathUtils.random(1,20), MathUtils.random(1,20));
            spawnedFood = true;
            for(IntVector2 i : snake.getComponent(PathComponent.class).points) {
                if(i.x == pos.x && i.y == pos.y) spawnedFood = false;
            }
            for(Entity block : blocks) {
                BlockComponent bc = block.getComponent(BlockComponent.class);
                if(bc.pos.x == pos.x && bc.pos.y == pos.y) spawnedFood = false;
            }
        }
        System.out.println(pos.x + "|" + pos.y);
        Entity newFoood= new Entity();
        newFoood.add(new FoodComponent(1));
        newFoood.add(new PositionComponent(pos.x, pos.y));
        engine.addEntity(newFoood);

        this.foodCounter++;
        if(foodCounter> GlobalSettings.FOOD_TO_NEXT_LEVEL && ECS.gameState.mode == GameMode.LEVELS
                && ECS.gameState.mode == GameMode.LEVELS)
            ECS.gameState.setLevelUp();
    }

    @Override
    public void entityAdded(Entity entity) {
        this.addedToEngine(getEngine());
    }

    @Override
    public void entityRemoved(Entity entity) {
        this.addedToEngine(getEngine());
    }
}
