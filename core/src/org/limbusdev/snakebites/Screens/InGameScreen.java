package org.limbusdev.snakebites.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import org.limbusdev.snakebites.Model.GameState;
import org.limbusdev.snakebites.Model.ThemePreset;
import org.limbusdev.snakebites.SnakeBites;
import org.limbusdev.snakebites.Systems.EntityComponentSystem;
import org.limbusdev.snakebites.util.MediaManager;

/**
 * Created by georg on 12.05.16.
 */
public class InGameScreen implements Screen {

    /* ............................................................................ ATTRIBUTES .. */
    private final SnakeBites game;

    // Renderers and Cameras
    public  OrthographicCamera  camera;
    private Viewport            viewport;
    private SpriteBatch         batch;
    private ShapeRenderer       shpRend;
    private BitmapFont          font;
    private MediaManager        media;
    private ThemePreset         theme;
    private Music bgMusic;

    private EntityComponentSystem ECS;



    /* ........................................................................... CONSTRUCTOR .. */
    public InGameScreen(final SnakeBites game, ThemePreset preset, GameState gameState) {
        this.game = game;
        this.media = game.media;
        this.theme = preset;
        setUpRendering();
        this.ECS = new EntityComponentSystem(game, this, viewport, preset, gameState);
        this.shpRend = new ShapeRenderer();
        this.bgMusic = media.assets.get(media.music.get(gameState.level%10), Music.class);
        if(gameState.level==10) media.assets.get(media.music.get(10), Music.class);
        if(gameState.level==20) media.assets.get(media.music.get(11), Music.class);
        if(gameState.level==30) media.assets.get(media.music.get(12), Music.class);
        bgMusic.setLooping(true);
    }

    /* ........................................................................ LIBGDX METHODS .. */

    @Override
    public void show() {
        batch   = new SpriteBatch();
        shpRend = new ShapeRenderer();
        font    = new BitmapFont();
        font.setColor(Color.WHITE);
        setUpInputProcessor();
        //bgMusic.play();
    }

    /**
     * Called when the screen should render itself.
     *
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void render(float delta) {
        // Clear screen
        Gdx.gl.glClearColor(theme.bgColor.r, theme.bgColor.g, theme.bgColor.b, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        updateCamera();

        ECS.update(delta);

        // ............................................................................... RENDERING


        ECS.render(this.batch, this.shpRend);


        // ............................................................................... RENDERING

    }

    /**
     * @param width
     * @param height
     */
    @Override
    public void resize(int width, int height) {
        viewport.update(width,height,true);
        ECS.resize(width,height);

    }

    @Override
    public void pause() {
        ECS.gameState.setPaused(true);
    }

    @Override
    public void resume() {
        // TODO
    }

    @Override
    public void hide() {
        bgMusic.stop();
    }

    /* ............................................................................... METHODS .. */

    private void setUpRendering() {
        // Rendering ...............................................................................
        camera   = new OrthographicCamera();    // set up the camera and viewport
        viewport = new FitViewport(720, 1280, camera);
        viewport.apply();
        camera.position.set(camera.viewportWidth/2, camera.viewportHeight/2, 0); // center camera

        batch   = new SpriteBatch();
        shpRend = new ShapeRenderer();
        font    = new BitmapFont();
        font.setColor(Color.WHITE);
    }

    /**
     * Called when this screen should release all resources.
     */
    @Override
    public void dispose() {
        this.shpRend.dispose();
        this.batch.dispose();
        this.font.dispose();
        bgMusic.stop();
        bgMusic.dispose();
    }

    public void setUpInputProcessor() {
        this.ECS.setInputProcessor();
    }

    private void updateCamera() {
        // project to camera
        batch.  setProjectionMatrix(camera.combined);
        shpRend.setProjectionMatrix(camera.combined);
        camera.update();
    }


}
