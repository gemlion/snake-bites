package org.limbusdev.snakebites.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.viewport.FitViewport;

import org.limbusdev.snakebites.Model.GameMode;
import org.limbusdev.snakebites.Model.GameState;
import org.limbusdev.snakebites.Model.ThemePreset;
import org.limbusdev.snakebites.SnakeBites;
import org.limbusdev.snakebites.util.GlobalSettings;

public class MainMenuScreen implements Screen {

    /* ............................................................................ ATTRIBUTES .. */
    public final SnakeBites game;

    // Scene2D.ui
    private Skin skin;
    private Stage stage;
    private ArrayMap<String, Button> buttons;;
    private Group startMenu, logoScreen, creditsScreen, introScreen;
    private boolean fistStart;
    private Texture snakeTitleImg;
    private Music bgMusic;
    private TextButton.TextButtonStyle buttonStyle;
    private GameMode gameMode;

    private TextureAtlas uiTA;

    /* ........................................................................... CONSTRUCTOR .. */
    public MainMenuScreen(final SnakeBites game, boolean firstStart) {
        this.fistStart = firstStart;
        this.game = game;
        this.uiTA = game.media.getTextureAtlas(game.media.controls);
        this.snakeTitleImg = game.media.assets.get(game.media.snakeTitleImg, Texture.class);
        snakeTitleImg.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);
        this.gameMode = GameMode.LEVELS;


        if(firstStart) setUpIntro();

        setUpUI();
        setUpStartMenu();
        if(firstStart) stage.addActor(introScreen);

        this.bgMusic = game.media.assets.get(game.media.music.get(0), Music.class);
        this.bgMusic.setLooping(true);
    }

    public MainMenuScreen(final SnakeBites game, GameMode mode) {
        this(game, false);
        this.gameMode = mode;
    }

    /* ............................................................................... METHODS .. */
    @Override
    public void show() {
        if(this.fistStart) {
            introScreen.addAction(Actions.sequence(
                    Actions.fadeIn(1), Actions.delay(1), Actions.fadeOut(1), Actions.visible(false)
            ));
            logoScreen.addAction(Actions.sequence(
                    Actions.delay(3), Actions.visible(true), Actions.fadeIn(1)
            ));
        } else {
            startMenu.addAction(Actions.sequence(
                    Actions.alpha(0), Actions.visible(true), Actions.fadeIn(1)
            ));
        }
        //this.bgMusic.play();
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.7f, 0.92f, 0.95f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // UI
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void pause() {
        // TODO
    }

    @Override
    public void resume() {
        // TODO
    }

    @Override
    public void hide() {
        bgMusic.stop();
    }

    @Override
    public void dispose() {
        this.bgMusic.stop();
        this.bgMusic.dispose();
    }

    public void setUpGame(ThemePreset theme) {
        GameState gameState = new GameState(gameMode);
        game.setScreen(new InGameScreen(game, theme, gameState));
    }


    public void setUpUI() {

        // Scene2D
        FitViewport fit = new FitViewport(GlobalSettings.SCREEN_WIDTH, GlobalSettings.SCREEN_HEIGHT);
        this.stage = new Stage(fit);
        Gdx.input.setInputProcessor(stage);
        this.skin = game.media.skin;

        this.logoScreen = new Group();

        Image logo = new Image(game.media.getTextureAtlas(game.media.controls).findRegion("logo"));
        logo.setPosition(GlobalSettings.SCREEN_WIDTH / 2, GlobalSettings.SCREEN_HEIGHT*2/3, Align.center);
        logoScreen.addActor(logo);


        // ----------------------------------------------------------------------------- LOGO SCREEN
        // Buttons .................................................................................
        buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = skin.getFont("default-font");
        buttonStyle.unpressedOffsetY = +1;
        buttonStyle.down = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("buttonDown"));
        buttonStyle.up = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("buttonUp"));

        // Start
        TextButton button = new TextButton("Levels", buttonStyle);
        button.setWidth(256);
        button.setHeight(96);
        button.setPosition(GlobalSettings.SCREEN_WIDTH / 2 - 128f, 330f);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameMode = GameMode.LEVELS;
                logoScreen.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.visible(false)
                ));
                startMenu.addAction(Actions.sequence(
                        Actions.alpha(0), Actions.visible(true), Actions.delay(1), Actions.fadeIn(1)
                ));
            }
        });
        logoScreen.addActor(button);

        // Options
        button = new TextButton("Endless", buttonStyle);
        button.setWidth(256);
        button.setHeight(96);
        button.setPosition(GlobalSettings.SCREEN_WIDTH / 2 - 128f, 220f);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameMode = GameMode.ENDLESS;
                logoScreen.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.visible(false)
                ));
                startMenu.addAction(Actions.sequence(
                        Actions.alpha(0), Actions.visible(true), Actions.delay(1), Actions.fadeIn(1)
                ));
            }
        });
        logoScreen.addActor(button);

        // Credits
        button = new TextButton("Credits", buttonStyle);
        button.setWidth(256);
        button.setHeight(96);
        button.setPosition(GlobalSettings.SCREEN_WIDTH / 2 - 128f, 110f);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                logoScreen.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.visible(false)
                ));
                stage.addActor(creditsScreen);
                creditsScreen.addAction(Actions.sequence(
                        Actions.alpha(0), Actions.visible(true), Actions.fadeIn(1)
                ));
            }
        });
        logoScreen.addActor(button);
        setUpCredits();

        // Buttons ............................................................................. END
        logoScreen.setVisible(false);
        logoScreen.addAction(Actions.alpha(0));
        logoScreen.act(1);
        stage.addActor(logoScreen);
    }


    /**
     * Set up start menu with level starters
     */
    public void setUpStartMenu() {
        this.startMenu = new Group();

        Image im = new Image(snakeTitleImg);
        im.setWidth(snakeTitleImg.getWidth());
        im.setHeight(snakeTitleImg.getHeight());
        im.setPosition(0,GlobalSettings.SCREEN_HEIGHT-snakeTitleImg.getHeight());
        startMenu.addActor(im);

        // ................................................................................. BUTTONS
        this.buttons = new ArrayMap<String, Button>();

        // ............................................................................ START BUTTON
        Button.ButtonStyle buttonToonStyle = new Button.ButtonStyle();
        buttonToonStyle.up = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("startToon_up"));
        buttonToonStyle.down = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("startToon_down"));
        Button button = new Button(buttonToonStyle);
        button.setWidth(192);
        button.setHeight(197);
        button.setPosition(
                GlobalSettings.SCREEN_WIDTH*1/5, GlobalSettings.SCREEN_HEIGHT/3, Align.center);

        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                setUpGame(game.media.toonPreset);
                            }
                        })
                ));
            }
        });
        buttons.put("toon", button);

        Button.ButtonStyle buttonRetroStyle = new Button.ButtonStyle();
        buttonRetroStyle.up = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("startRetro_up"));
        buttonRetroStyle.down = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("startRetro_down"));
        button = new Button(buttonRetroStyle);
        button.setWidth(192);
        button.setHeight(197);
        button.setPosition(GlobalSettings.SCREEN_WIDTH / 2, GlobalSettings.SCREEN_HEIGHT/3, Align.center);

        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                setUpGame(game.media.retroPreset);
                            }
                        })
                ));
            }
        });
        buttons.put("pixels", button);

        Button.ButtonStyle buttonPixelsStyle = new Button.ButtonStyle();
        buttonPixelsStyle.up = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("startPixels_up"));
        buttonPixelsStyle.down = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("startPixels_down"));
        button = new Button(buttonPixelsStyle);
        button.setWidth(192);
        button.setHeight(197);
        button.setPosition(GlobalSettings.SCREEN_WIDTH*4/5, GlobalSettings.SCREEN_HEIGHT/3, Align.center);


        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                setUpGame(game.media.pixelsPreset);
                            }
                        })
                ));
            }
        });
        buttons.put("retro", button);

        button = new TextButton("Back", buttonStyle);
        button.setWidth(256);
        button.setHeight(96);
        button.setPosition(GlobalSettings.SCREEN_WIDTH / 2 - 128f, 32f);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                startMenu.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.visible(false)
                ));
                logoScreen.addAction(Actions.sequence(
                        Actions.alpha(0), Actions.visible(true), Actions.delay(1), Actions.fadeIn(1)
                ));
            }
        });
        buttons.put("back", button);

        Button.ButtonStyle buttonAchievementStyle = new Button.ButtonStyle();
        buttonAchievementStyle.up = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("achieve"));
        button = new Button(buttonAchievementStyle);
        button.setWidth(80);
        button.setHeight(128);
        button.setPosition(GlobalSettings.SCREEN_WIDTH*1/5-40, 32f);
        button.addListener(new ClickListener() {
                               @Override
                               public void clicked(InputEvent event, float x, float y) {
                                   System.out.println("Show Achievements");
                               }
                           });
        buttons.put("achieve", button);

        Button.ButtonStyle buttonHighscoreStyle = new Button.ButtonStyle();
        buttonHighscoreStyle.up = new TextureRegionDrawable(
                game.media.getTextureAtlas(game.media.controls).findRegion("highscore"));
        button = new Button(buttonHighscoreStyle);
        button.setWidth(128);
        button.setHeight(85.3f);
        button.setPosition(GlobalSettings.SCREEN_WIDTH*4/5-64, 32f);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("Get Highscore");
            }
        });
        buttons.put("highscore", button);




        for (String key : buttons.keys()) startMenu.addActor(buttons.get(key));
        startMenu.addAction(Actions.alpha(0));
        startMenu.act(1);
        startMenu.setVisible(false);
        stage.addActor(startMenu);
    }


    public void setUpIntro() {
        this.introScreen = new Group();
        Image logo = new Image(game.media.getTextureAtlas(game.media.logos).findRegion("limbusdev"));
        logo.setWidth(512);
        logo.setHeight(89);
        logo.setPosition(GlobalSettings.SCREEN_WIDTH / 2, GlobalSettings.SCREEN_HEIGHT / 2, Align.center);
        introScreen.addActor(logo);
    }

    public void setUpCredits() {
        this.creditsScreen = new Group();
        Image limbusLogo = new Image(game.media.getTextureAtlas(game.media.logos).findRegion("limbusdev"));
        limbusLogo.setWidth(512);limbusLogo.setHeight(89);
        limbusLogo.setPosition(GlobalSettings.SCREEN_WIDTH / 2, 1000, Align.center);
        Image libgdxLogo = new Image(game.media.getTextureAtlas(game.media.logos).findRegion("libgdx"));
        libgdxLogo.setWidth(512);libgdxLogo.setHeight(86);
        libgdxLogo.setPosition(GlobalSettings.SCREEN_WIDTH / 2, 400, Align.center);

        String creditText = "Development & Artwork\n\n" +
                "Georg Eckert, LimbusDev 2016\n\n\n\n" +
                "Powered by\n\n";
        Label.LabelStyle labs = new Label.LabelStyle();
        labs.font = skin.getFont("default-font");
        Label text = new Label(creditText, labs);
        text.setAlignment(Align.top,Align.top);
        text.setPosition(60,200);
        text.setWidth(600);text.setHeight(600);
        text.setWrap(true);

        // Credits
        TextButton button = new TextButton("Back", buttonStyle);
        button.setWidth(256);
        button.setHeight(96);
        button.setPosition(GlobalSettings.SCREEN_WIDTH / 2 - 128f, 96f);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                creditsScreen.addAction(Actions.sequence(
                        Actions.fadeOut(1), Actions.visible(false)
                ));
                logoScreen.addAction(Actions.sequence(
                        Actions.alpha(0), Actions.visible(true), Actions.fadeIn(1)
                ));
            }
        });
        creditsScreen.addActor(button);

        // Sorting
        creditsScreen.addActor(text);
        creditsScreen.addActor(limbusLogo);
        creditsScreen.addActor(libgdxLogo);
        creditsScreen.setVisible(false);
    }

}