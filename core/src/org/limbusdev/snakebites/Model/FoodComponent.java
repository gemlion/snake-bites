package org.limbusdev.snakebites.Model;

import com.badlogic.ashley.core.Component;

/**
 * Created by georg on 13.05.16.
 */
public class FoodComponent implements Component {
    public int value;
    public FoodComponent(int value) {
        this.value = value;
    }
}
