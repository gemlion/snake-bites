package org.limbusdev.snakebites.Model;

/**
 * Created by georg on 13.05.16.
 */
public class IntVector2 {

    public int x,y;

    public IntVector2(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
