package org.limbusdev.snakebites.Model;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.LinkedList;

/**
 * Created by georg on 12.05.16.
 */
public class PathComponent implements Component {
    public LinkedList<IntVector2> points;
    public int inBelly=0;
    public Direction dir;

    public PathComponent (IntVector2 start, int length) {
        this.points = new LinkedList<IntVector2>();
        for (int i = 0; i < length; i++) {
            points.addLast(new IntVector2(start.x + i, start.y));
        }
        this.dir = Direction.E;
    }

    public void eat(int amount) {
        inBelly+=amount;
    }
}
