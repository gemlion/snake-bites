package org.limbusdev.snakebites.Model;

import com.badlogic.gdx.graphics.Color;

import org.limbusdev.snakebites.THEME;

/**
 * Created by georg on 16.05.16.
 */
public class ThemePreset {
    public Color bgColor;
    public String screenFontColor, buttonFontColor;
    public THEME theme;
    public String spriteSheetFile;


    public ThemePreset(Color bgColor, THEME theme, String spriteSheetFile, String screenFontColor, String buttonFontColor) {
        this.bgColor = bgColor;
        this.theme = theme;
        this.spriteSheetFile = spriteSheetFile;
        this.screenFontColor = screenFontColor;
        this.buttonFontColor = buttonFontColor;
    }
}
