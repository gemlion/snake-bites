package org.limbusdev.snakebites.Model;

import com.badlogic.ashley.core.Component;

/**
 * Created by georg on 12.05.16.
 */
public class InputComponent implements Component {
    public Direction dir;
    public boolean   active;

    public InputComponent(Direction dir) {
        this.dir = dir;
        this.active = false;
    }
}
