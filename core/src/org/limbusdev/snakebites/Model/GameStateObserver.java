package org.limbusdev.snakebites.Model;

/**
 * Created by georg on 16.05.16.
 */
public interface GameStateObserver {

    public void getNotified(GameState gameState);

}
