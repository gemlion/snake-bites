package org.limbusdev.snakebites.Model;

import com.badlogic.ashley.core.Component;

/**
 * Created by georg on 16.05.16.
 */
public class BlockComponent implements Component {
    public IntVector2 pos;

    public BlockComponent(IntVector2 pos) {
        this.pos = pos;
    }
}
