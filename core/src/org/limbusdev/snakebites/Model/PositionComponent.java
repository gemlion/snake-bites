package org.limbusdev.snakebites.Model;

import com.badlogic.ashley.core.Component;

/**
 * Created by georg on 13.05.16.
 */
public class PositionComponent implements Component {
    public int x,y;
    public PositionComponent(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
