package org.limbusdev.snakebites.Model;

import com.badlogic.gdx.utils.Array;

import java.util.Observable;

/**
 * Created by georg on 16.05.16.
 */
public class GameState {
    public boolean paused, gameOver, levelUp;
    public int score, level;
    private Array<GameStateObserver> observers;
    public GameMode mode;

    public GameState() {
        this.paused = false;
        this.gameOver = false;
        this.levelUp = false;
        this.score = 0;
        this.level = 1;
        this.observers = new Array<GameStateObserver>();
        this.mode = GameMode.LEVELS;
    }

    public GameState(GameMode mode) {
        this();
        this.mode = mode;
    }

    public void addObserver(GameStateObserver gso) {
        this.observers.add(gso);
    }

    public void notifyObservers() {
        for(GameStateObserver gso : observers) {
            gso.getNotified(this);
        }
    }

    public void raiseScore(int score) {
        this.score+=score;
        notifyObservers();
    }

    public void setPaused(boolean on) {
        this.paused = on;
        notifyObservers();
    }

    public void setGameOver(boolean on) {
        this.gameOver = on;
        this.setPaused(true);
        notifyObservers();
    }
    public void setLevelUp() {
        this.level++;
        this.levelUp=true;
        this.setPaused(true);
    }

}
