package org.limbusdev.snakebites;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import org.limbusdev.snakebites.Screens.MainMenuScreen;
import org.limbusdev.snakebites.util.MediaManager;

public class SnakeBites extends Game {

    public MediaManager media;

	
	@Override
	public void create () {
        AssetManager assets = new AssetManager();
        this.media = new MediaManager(assets);
		this.setScreen(new MainMenuScreen(this, true));
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void dispose() {
        // TODO
	}

	public void gameOver() {
		this.setScreen(new MainMenuScreen(this, false));
	}
}
