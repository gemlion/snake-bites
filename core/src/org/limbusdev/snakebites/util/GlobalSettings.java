package org.limbusdev.snakebites.util;

import org.limbusdev.snakebites.THEME;

/**
 * Created by georg on 13.05.16.
 */
public class GlobalSettings {
    public static final THEME theme = THEME.PIXELS;
    public static final int SCREEN_WIDTH = 720;
    public static final int SCREEN_HEIGHT = 1280;
    public static final int MAX_LEVEL = 14;
    public static final int FOOD_TO_NEXT_LEVEL = 25;
    public static final int MAX_TIME_BETWEEN_STEPS = 250;
    public static final int MIN_TIME_BETWEEN_STEPS = 30;
    public static final int TIME_BETWEEN_STEPS_DECREASE = 4;
}
