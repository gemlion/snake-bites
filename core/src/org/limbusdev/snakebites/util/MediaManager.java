package org.limbusdev.snakebites.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

import org.limbusdev.snakebites.Model.ThemePreset;
import org.limbusdev.snakebites.THEME;

/**
 * Created by georg on 13.05.16.
 */
public class MediaManager {
    public AssetManager assets;
    public final static String snakeToon = "snakeToon.pack";
    public final static String snakePixels = "snakePixels.pack";
    public final static String snakeRetro = "retro.pack";
    public final static String logos = "logos.pack";
    public final static String controls  = "controls.pack";
    public final static String UISpriteSheetFile = "UI.pack";
    public final Skin skin;
    public final static String snakeTitleImg = "snakeTitle.png";
    public final Array<String> sfx;
    public final Array<String> music;

    public final ThemePreset toonPreset, retroPreset, pixelsPreset;

    public MediaManager(AssetManager assets) {
        this.sfx = new Array<String>();
        this.music = new Array<String>();
        this.assets = assets;
        this.assets.load(snakeToon, TextureAtlas.class);
        this.assets.load(snakePixels, TextureAtlas.class);
        this.assets.load(snakeRetro, TextureAtlas.class);
        this.assets.load(controls, TextureAtlas.class);
        this.assets.load(logos, TextureAtlas.class);
        this.assets.load(UISpriteSheetFile, TextureAtlas.class);
        this.assets.load(snakeTitleImg, Texture.class);

        // Music ............................................................................. MUSIC
        sfx.add("sfx/food.wav");
        sfx.add("sfx/pause_in.wav");
        sfx.add("sfx/pause_out.wav");
        sfx.add("sfx/win.wav");
        sfx.add("sfx/loose.wav");
        for(String s : sfx) this.assets.load(s, Sound.class);

        music.add("music/menu.ogg");
        for(int i=1;i<10;i++) music.add("music/level" + i + ".ogg");
        music.add("music/level10.ogg");
        music.add("music/level20.ogg");
        music.add("music/level30.ogg");
        music.add("music/credits.ogg");
        for(String s : music) this.assets.load(s, Music.class);

        // Fonts ............................................................................. FONTS
        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal
                ("fonts/Cloudy-With-a-Chance-of-Love.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator
                .FreeTypeFontParameter();
        param.color = Color.BLACK;
        param.size = 32;
        param.magFilter = Texture.TextureFilter.Nearest;
        param.minFilter = Texture.TextureFilter.Linear;
        BitmapFont font32 = gen.generateFont(param);
        param.color = Color.WHITE;
        param.size = 32;
        param.magFilter = Texture.TextureFilter.Nearest;
        param.minFilter = Texture.TextureFilter.Linear;
        BitmapFont font32w = gen.generateFont(param);
        gen.dispose();

        this.skin = new Skin();

        skin.addRegions(new TextureAtlas(Gdx.files.internal("scene2d/uiskin.atlas")));
        skin.add("default-font", font32);
        skin.add("white", font32w);

        skin.load(Gdx.files.internal("scene2d/uiskin.json"));

        this.assets.finishLoading();

        this.retroPreset    = new ThemePreset(new Color(0.46f,0.46f,0.46f,1), THEME.RETRO,  this.snakeRetro, "default-font", "default-font");
        this.pixelsPreset   = new ThemePreset(new Color(1f,  .97f,  0f,   1), THEME.PIXELS, this.snakePixels, "default-font", "white");
        this.toonPreset     = new ThemePreset(new Color(1f, 0.8f, 0.2f,   1), THEME.TOON,   this.snakeToon, "white", "default-font");
    }

    public TextureAtlas getTextureAtlas(String name) {
        return assets.get(name, TextureAtlas.class);
    }
}
