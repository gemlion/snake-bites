# README #

Welcome, you've come to the home of Snake Bites - The free and Open Source Snake Game for Android and PCs capable of Java.

# How to install #
* on Google Play Store soon
* clone and build the Android Studio Project for yourself

# Credits #
Made with libGDX, Blender, Imkscape, GIMP
Programming and Artwork by Georg Eckert

# Screenshots #
![SB1.png](https://bitbucket.org/repo/ykaya4/images/3174637000-SB1.png)
![SB2.png](https://bitbucket.org/repo/ykaya4/images/3074578885-SB2.png)
![SB3.png](https://bitbucket.org/repo/ykaya4/images/335112365-SB3.png)
![SB4.png](https://bitbucket.org/repo/ykaya4/images/2823517822-SB4.png)