package org.limbusdev.snakebites.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.limbusdev.snakebites.SnakeBites;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "MonsterWorld";
		config.width = 480;
		config.height = 800;
		new LwjglApplication(new SnakeBites(), config);
	}
}
